<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiseasesByYear extends Model
{
    use HasFactory;
    protected $fillable = ['year', 'diseases'];
}
