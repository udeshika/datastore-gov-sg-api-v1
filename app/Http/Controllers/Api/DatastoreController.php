<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Datastore;
use App\Models\DiseasesByYear;
use App\Models\DiseasesByWeek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DatastoreController extends Controller
{
    public function __invoke(Request $request)
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pull(Request $request)
    {
        $post = $request->post();
        $offset   = (isset($post['offset']) ? $post['offset'] : "0");
        $limit   = (isset($post['limit']) ? $post['limit'] : "10");
        $response = Http::get('https://data.gov.sg/api/action/datastore_search', [
            'resource_id' => 'ef7e44f1-9b14-4680-a60a-37d2c9dda390',
            'offset' =>  $offset,
            'limit' => $limit
        ]);

        if ($response) {
            $responseData = $response['result']['records'];
        }

        $year_data_arr = [];
        $i = 0;
        foreach ($responseData as $key => $value) {

            DiseasesByWeek::upsert(
                ['week' => $value['epi_week'], 'diseases' => $value['disease'], 'no_of_cases' => $value['no._of_cases']],
                ['year', 'diseases'],
                ['no_of_cases']
            );

            if (!isset($year_data[substr($value['epi_week'], 0, 4)][$value['disease']]['no_of_cases'])) {
                $year_data_arr[substr($value['epi_week'], 0, 4)][$i]['no_of_cases'] = 0;
            }

            $year_data_arr[substr($value['epi_week'], 0, 4)][$i]['disease'] = $value['disease'];
            $year_data_arr[substr($value['epi_week'], 0, 4)][$i]['no_of_cases'] += $value['no._of_cases'];
            $i++;
        }

        foreach ($year_data_arr as $year => $data) {

            foreach ($data as $diseases_data) {

                DiseasesByYear::upsert(
                    ['year' => $year, 'diseases' => $diseases_data['disease'], 'no_of_cases' => $diseases_data['no_of_cases']],
                    ['year', 'diseases'],
                    ['no_of_cases']
                );
            }
        }

        $message = 'Table updated from data.gov.sg';
        $success['status'] = true;

        return sendResponse($success, $message, 201);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function diseases()
    {

        $data_diseases = DiseasesByYear::select(["diseases as value", "diseases as label"])
            ->groupBy("diseases")
            ->get();

        $message = '';
        $success['status'] = true;
        $success  =  $data_diseases;
        return sendResponse($success, $message, 201);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function years()
    {
        $data_by_year = DiseasesByYear::select(["year as value", "year as label"])
            ->groupBy("year")
            ->get();

        $message = '';
        $success['status'] = true;
        $success  =  $data_by_year;
        return sendResponse($success, $message, 201);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function weeks(Request $request)
    {
        $post = $request->post();
        if (isset($post['year'])) :
            $data_by_year = DiseasesByWeek::select(["week as value", "week as label"])
                ->where("week", "LIKE", '%' .  $post['year'] . '%')
                ->groupBy("week")
                ->get();
        else :
            $data_by_year = DiseasesByWeek::select(["week as value", "week as label"])
                ->groupBy("week")
                ->get();
        endif;
        $message = '';
        $success['status'] = true;
        $success  =  $data_by_year;
        return sendResponse($success, $message, 201);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $post = $request->post();
        $year   = @$post['year'];
        $week   = @$post['week'];
        $diseases   = @$post['diseases'];

        $success['year'] =    $year;
        $success['week'] =    $week;
        $success['diseases'] =    $diseases;
        if ($week) {

            $data_by = DiseasesByWeek::select(["diseases as label", "no_of_cases as value"])
                ->where("week", "=", $week)
                ->get();
        } else {
            $data_by = DiseasesByYear::select(["diseases as label", "no_of_cases as value"])
                ->where("year", "=", $year)
                ->get();
        }
        $message = '';
        $success['status'] = true;
        $success  =  $data_by;
        return sendResponse($success, $message, 201);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function weeklyData(Request $request)
    {
        $post = $request->post();
        $diseases   = $post['diseases'];
        $message = '';
        $data_by = [];
        if ($diseases) {

            $data_by = DiseasesByWeek::select(["week as label", "no_of_cases as value"])
                ->where("diseases", "=", $diseases)
                ->get();
        }
        $message = '';
        $success['status'] = true;
        $success  =  $data_by;
        return sendResponse($success, $message, 201);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $success['token'] = [];
        $message = 'Oops! Unable to create a new user.';
        return sendResponse($success, $message, 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Datastore  $Datastore
     * @return \Illuminate\Http\Response
     */
    public function show(Datastore $Datastore)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Datastore  $Datastore
     * @return \Illuminate\Http\Response
     */
    public function edit(Datastore $Datastore)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Datastore  $Datastore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datastore $Datastore)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Datastore  $Datastore
     * @return \Illuminate\Http\Response
     */
    public function destroy(Datastore $Datastore)
    {
    }
}
