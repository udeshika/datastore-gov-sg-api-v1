<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\DatastoreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [LoginController::class, 'register']);
    Route::post('tokenlogin', [LoginController::class, 'tokenlogin']);

    Route::middleware('auth:api')->group(function () {
    });

    Route::post('datastore/search', [DatastoreController::class, 'search'])->name('datastore-search');;
    Route::post('datastore/diseases', [DatastoreController::class, 'diseases'])->name('datastore-diseases-filter');
    Route::post('datastore/weekly_data', [DatastoreController::class, 'weeklyData'])->name('datastore-list-weekly-data');
    Route::get('datastore/years', [DatastoreController::class, 'years'])->name('datastore-years-list');
    Route::post('datastore/weeks', [DatastoreController::class, 'weeks'])->name('datastore-weeks-list');
    Route::post('datastore/pull', [DatastoreController::class, 'pull'])->name('datastore-data-pull');
    Route::get('datastore', [DatastoreController::class, 'index']);
});
