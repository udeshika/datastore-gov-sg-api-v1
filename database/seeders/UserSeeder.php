<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' =>  'api@gmail.com',
            'password' => Hash::make('12345678'),
            'remember_token' =>  "apitest",
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        User::factory()
            ->count(5)
            ->create();
    }
}
