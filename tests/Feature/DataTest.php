<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DataTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_can_search_by_year()
    {

        $data = [
            'year' => "2013",
            'week' => "",
            'diseases' => "",
        ];



        $this->post(route('datastore-search'), $data)
            ->assertStatus(200)
            ->assertJsonFragment(["label" => "Acute Viral hepatitis B"]);
    }
    public function test_can_search_by_week()
    {

        $data = [
            'year' => "",
            'week' => "2013-W13",
            'diseases' => "",
        ];



        $this->post(route('datastore-search'), $data)
            ->assertStatus(200)
            ->assertJsonFragment(["label" => "Meningococcal Infection"]);
    }

    public function test_can_list_years()
    {




        $this->get(route('datastore-years-list'))
            ->assertStatus(200)
            ->assertJsonFragment(["value" => 2014, "label" => 2014]);
    }


    public function test_can_list_weeks()
    {

        $data = [
            'year' => "2013",
            'week' => "",
            'diseases' => "",
        ];



        $this->post(route('datastore-weeks-list'), $data)
            ->assertStatus(200)
            ->assertJsonFragment(["value" => "2013-W13", "label" => "2013-W13"]);
    }


    public function test_can_list_weekly_data()
    {
        $data = [
            'diseases' => "Plague",
        ];

        $this->post(route('datastore-list-weekly-data'), $data)
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }

    public function test_can_data_pull()
    {
        $this->post(route('datastore-data-pull'))
            ->assertStatus(200)
            ->assertJsonFragment(["message" => "Table updated from data.gov.sg"]);
    }
}
