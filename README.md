## Laravel 8 REST API for Tech Assignment Building A Infectious Disease Bulletin

#### Step 1: Install Laravel

`composer install`

#### Step 2: Database Configuration

Create a database and configure the env file.
`php artisan migrate`
`php artisan migrate:refresh --seed`

#### Step 3: Passport Installation

To get started, install Passport via the Composer package manager:

`composer require laravel/passport`
`php artisan passport:install`
The Passport service provider registers its package own database migration directory with the framework.

Migrate database after installing the package.

The Passport migrations will create the tables in application related db must store clients access tokens:

`php artisan migrate`
`php artisan migrate:refresh --seed`
Run `passport:install` command. This command will create the encryption keys

Finally, in `config/auth.php` configuration file need to be look like this

```
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'passport',
        'provider' => 'users',
    ],
],
```

#### Step 4: API Routes

Routs definition is located in routes/api.php prefix can be use any

**routes/api.php**

```
<?php


Route::prefix('v1')->group(function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [LoginController::class, 'register']);
    Route::post('tokenlogin', [LoginController::class, 'tokenlogin']);

    Route::middleware('auth:api')->group(function () {
    });

    Route::post('datastore/search', [DatastoreController::class, 'search'])->name('datastore-search');;
    Route::post('datastore/diseases', [DatastoreController::class, 'diseases'])->name('datastore-diseases-filter');
    Route::post('datastore/weekly_data', [DatastoreController::class, 'weekly_data'])->name('datastore-list-weekly-data');
    Route::get('datastore/years', [DatastoreController::class, 'years'])->name('datastore-years-list');
    Route::post('datastore/weeks', [DatastoreController::class, 'weeks'])->name('datastore-weeks-list');
    Route::post('datastore/pull', [DatastoreController::class, 'pull'])->name('datastore-data-pull');
    Route::get('datastore', [DatastoreController::class, 'index']);
});
```

#### Step 5: Helper Functions

This helper was created to return responce in same fromat including success status
**app/Helpers/Functions.php**

```
<?php


   function sendResponse($result, $message)
   {
       $response = [
           'success' => true,
           'data'    => $result,
           'message' => $message,
       ];

       return response()->json($response, 200);
   }


   function sendError($error, $errorMessages = [], $code = 404)
   {
       $response = [
           'success' => false,
           'message' => $error,
       ];

       !empty($errorMessages) ? $response['data'] = $errorMessages : null;

       return response()->json($response, $code);
   }
```

#### Step 6: Create Eloquent API Resources

Used eloquent api resources with api. this used in DatastoreController file.

Run bellow command for quick run:

`php artisan serve`

# REST API

Here is Routes URL with Verb:

Now simply run above listed url like:

```
- \*\*Data pull from data.gov.sg and upsert to local db weekly and yearly :POST, URL: {BASE_URL}/api/v1/datastore/pull

curl --request POST \
  --url http://127.0.0.1:8000/api/v1/datastore/pull \
  --header 'Content-Type: multipart/form-data' \
  --form offset=10 \
  --form limit=10
```

```

- \*\*Search by week:POST, URL: {BASE_URL}/api/v1/datastore/search

curl --request POST \
  --url http://127.0.0.1:8000/api/v1/datastore/search \
  --header 'Content-Type: multipart/form-data' \
  --form year= \
  --form week=2013-W13

```

```

- \*\*Get all years:POST, URL: {BASE_URL}/api/v1/datastore/years

curl --request GET \
  --url http://127.0.0.1:8000/api/v1/datastore/years \
  --header 'Content-Type: multipart/form-data' \
  --form year=2013 \
  --form week= \ 
  --form =

```

```

- \*\*Get all weeks:POST, URL: {BASE_URL}/api/v1/datastore/weeks

curl --request POST \
  --url http://127.0.0.1:8000/api/v1/datastore/weeks \
  --header 'Content-Type: multipart/form-data' \
  --form year=2013 \
  --form week= \
  --form =

```

```

- \*\*Get all weekly_data by diseases:POST, URL: {BASE_URL}/api/v1/datastore/weekly_data

curl --request POST \
  --url http://127.0.0.1:8000/api/v1/datastore/weekly_data \
  --header 'Content-Type: multipart/form-data' \
  --form = \
  --form diseases=Plague \
  --form =
```

Headers:

```

'headers' => [
'Accept' => 'application/json',
]

```

Unit Feature test for api calls

```

php artisan test
Tests\Feature\DataTest
✓ basic
✓ can search by year
✓ can search by week
✓ can list years
✓ can list weeks
✓ can list weekly data
✓ can data pull

```
